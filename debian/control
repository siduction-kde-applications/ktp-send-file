Source: ktp-send-file
Section: kde
Priority: optional
Maintainer: Debian KDE Extras Team <pkg-kde-extras@lists.alioth.debian.org>
Uploaders: Diane Trout <diane@ghic.org>,
           Michał Zając <quintasan@kubuntu.org>,
           Mark Purcell <msp@debian.org>
Build-Depends: cmake (>= 2.8.12~),
               debhelper (>= 9),
               extra-cmake-modules (>= 1.3.0~),
               kdelibs5-dev (>= 4:4.7),
               kio-dev (>= 5.0~),
               libkf5i18n-dev (>= 5.0~),
               libkf5iconthemes-dev (>= 5.0~),
               libkf5kcmutils-dev (>= 5.0~),
               libktp-dev,
               libktpcommoninternalsprivate-dev (>= 0.8.80),
               libtelepathy-qt4-dev (>= 0.9.3),
               pkg-kde-tools (>= 0.9),
               qtbase5-dev (>= 5.4)
Standards-Version: 3.9.5
Homepage: https://projects.kde.org/projects/extragear/network/telepathy/ktp-send-file
Vcs-Git: git://anonscm.debian.org/pkg-kde/kde-extras/kde-telepathy/ktp-send-file.git
Vcs-Browser: http://anonscm.debian.org/gitweb/?p=pkg-kde/kde-extras/kde-telepathy/ktp-send-file.git

Package: kde-telepathy-send-file
Architecture: any
Depends: kde-telepathy-data (>= ${source:Upstream-Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: kde-telepathy
Enhances: dolphin, konqueror
Description: KDE Telepathy dolphin/konqueror integration plugin
 This package provides a dolphin/konqueror plugin that allows you
 to easily send files to your contacts from a context menu action.
 .
 This package is not meant to be used standalone. It is recommended
 to install the kde-telepathy metapackage instead.

Package: kde-telepathy-send-file-dbg
Architecture: any
Section: debug
Priority: extra
Depends: kde-telepathy-send-file (= ${binary:Version}), ${misc:Depends}
Description: KDE Telepathy dolphin/konqueror integration plugin - debug symbols
 This package provides the debug symbols for the KDE Telepathy send file
 plugin.
